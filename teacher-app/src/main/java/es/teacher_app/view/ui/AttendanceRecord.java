package es.teacher_app.view.ui;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;
import com.google.gson.JsonElement;
import com.pubnub.api.PNConfiguration;
import com.pubnub.api.*;
import com.pubnub.api.callbacks.PNCallback;
import com.pubnub.api.models.consumer.PNStatus;
import com.pubnub.api.models.consumer.history.PNHistoryItemResult;
import com.pubnub.api.models.consumer.history.PNHistoryResult;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import es.teacher_app.R;

import static android.R.id.input;

public class AttendanceRecord extends Fragment {

    private View view;
    private Button calendar_start;
    private Button calendar_end;
    private Spinner subjects_spinner;
    private Spinner students_spinner;
    private ListView list;
    private List<String> subjects = new ArrayList<>();
    private List<String> students = new ArrayList<>();
    private List<String> attendance = new ArrayList<>();
    private ArrayAdapter<String> dataAdapter;
    private long startTime, endTime;
    private String androidId;
    private String startDate;
    private String endDate;


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_attendance_record, container, false);

        androidId = Settings.Secure.getString(getActivity().getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);

        initializeElements();

        try {
            getStartCalendar();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return view;
    }



    private void initializeElements() {
        calendar_start = (Button) view.findViewById(R.id.calendar_start_spinner);
        calendar_end = (Button) view.findViewById(R.id.calendar_end_spinner);
        subjects_spinner = (Spinner) view.findViewById(R.id.subject_spinner);
        students_spinner = (Spinner) view.findViewById(R.id.student_spinner);
        list = (ListView) view.findViewById(R.id.lstOptions);
    }

    private void getDataFromPubNub(final long startTime, long endTime) {
        PNConfiguration pnConfiguration = new PNConfiguration();
        pnConfiguration.setSubscribeKey("sub-c-b2c3168a-45f6-11e7-b730-0619f8945a4f");
        pnConfiguration.setPublishKey("pub-c-e9ba54f6-4e07-48aa-8a0e-087d3d0fc6d6");
        pnConfiguration.setSecure(false);

        PubNub pubnub = new PubNub(pnConfiguration);

        pubnub.history()
                .channel("activityRecord4") // where to fetch history from
                .count(100) // how many items to fetch
                .start(startTime)
                .end(endTime)
                .async(new PNCallback<PNHistoryResult>() {
                    @Override
                    public void onResponse(PNHistoryResult result, PNStatus status) {
                        List<PNHistoryItemResult> messages = result.getMessages();

                        System.out.println("Message"+messages.size());
                        String subject = getMySubject(messages);
                        System.out.println("Subject is"+subject);
                    }
                });

    }


    private String getMySubject(List<PNHistoryItemResult> messages){
        String subject = "";

        if(messages.size() == 0){
            getSpinnerSubjects(subject, messages);
        }

        for (int i = 0; i < messages.size(); i++) {
            if(messages.get(i).getEntry().isJsonObject()) {
                JsonElement jsonElement = messages.get(i).getEntry().getAsJsonObject().get("such");

                System.out.println("Items"+jsonElement);

                if (jsonElement.isJsonArray()) {
                    for (int j = 0; j < jsonElement.getAsJsonArray().size(); j++) {
                        if (jsonElement.getAsJsonArray().get(j).getAsJsonObject().get("status").toString().equals("1") &&
                                jsonElement.getAsJsonArray().get(j).getAsJsonObject().get("id").toString().replaceAll("\"", "").equals(androidId)) {

                            subject = jsonElement.getAsJsonArray().get(j).getAsJsonObject().get("subject").toString().replaceAll("\"", "");

                            System.out.println("Subjectt"+subject);

                            getSpinnerSubjects(subject, messages);
                        }
                    }
                }
            }
        }
        return subject;
    }

    private void getSpinnerStudents(final String subjectSelected, List<String> students, final List<PNHistoryItemResult> messages) {
        dataAdapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_spinner_item, students);

        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        students_spinner.setAdapter(dataAdapter);

        students_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                Toast.makeText(parent.getContext(), "Selected!", Toast.LENGTH_SHORT).show();
                String studentSelected = parent.getItemAtPosition(position).toString();

                if(studentSelected != null){
                    attendance.clear();
                }

                try {
                    attendance = getDataForListView(subjectSelected,studentSelected, messages);

                    ArrayAdapter<String> adapter =
                            new ArrayAdapter<>(getActivity(),
                                    android.R.layout.simple_list_item_1, attendance);
                    list.setAdapter(adapter);


                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });
    }

    private void getSpinnerSubjects(String subject, final List<PNHistoryItemResult> messages) {
        if(!subjects.contains(subject) && !subject.equals("")){
            subjects.add(subject);
        }

        if(subject.equals("")){
            subjects.clear();
            students.clear();
            students_spinner.setAdapter(null);
            list.setAdapter(null);
        }

        dataAdapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_spinner_item, subjects);

        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        subjects_spinner.setAdapter(dataAdapter);

        subjects_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                Toast.makeText(parent.getContext(), "Selected!", Toast.LENGTH_SHORT).show();
                String subjectSelected = parent.getItemAtPosition(position).toString();

                if(subjectSelected != null){
                    students.clear();
                }

                students = getStudentsConnectedToSubject(subjectSelected, messages);
                getSpinnerStudents(subjectSelected, students, messages);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });
    }

    private List<String> getStudentsConnectedToSubject(String subject, List<PNHistoryItemResult> messages){
        if(messages.size() == 0){
            students.clear();
        }
        for (int i = 0; i < messages.size(); i++) {
            if(messages.get(i).getEntry().isJsonObject()){
                JsonElement jsonElement = messages.get(i).getEntry().getAsJsonObject().get("such");
                if(jsonElement.isJsonArray()){
                    for (int j = 0; j < jsonElement.getAsJsonArray().size(); j++) {

                        if(jsonElement.getAsJsonArray().get(j).getAsJsonObject().get("status").toString().equals("0")
                                && subject.equals(jsonElement.getAsJsonArray().get(j).getAsJsonObject().get("subject").toString().replaceAll("\"", ""))) {

                            if (jsonElement.getAsJsonArray().get(j).getAsJsonObject().get("name") != null) {

                                String student = jsonElement.getAsJsonArray().get(j).getAsJsonObject().get("name").toString().replaceAll("\"", "");

                                if (!students.contains(student)) {
                                    students.add(student);
                                }
                            }
                        }
                    }
                }
            }
        }
        return students;
    }

    private List<String> getDataForListView(String subject, String student, List<PNHistoryItemResult> messages) throws ParseException {
        String concat = "";
        for (int i = 0; i < messages.size(); i++) {
            if(messages.get(i).getEntry().isJsonObject()){
                JsonElement jsonElement = messages.get(i).getEntry().getAsJsonObject().get("such");
                if(jsonElement.isJsonArray()){
                    for (int j = 0; j < jsonElement.getAsJsonArray().size(); j++) {
                        if(jsonElement.getAsJsonArray().get(j).getAsJsonObject().get("status").toString().equals("0")
                                && subject.equals(jsonElement.getAsJsonArray().get(j).getAsJsonObject().get("subject").toString().replaceAll("\"", "")) &&
                                student.equals(jsonElement.getAsJsonArray().get(j).getAsJsonObject().get("name").toString().replaceAll("\"", ""))) {

                            String date = jsonElement.getAsJsonArray().get(j).getAsJsonObject().get("date").toString().replaceAll("\"", "");
                            String studentSubject = jsonElement.getAsJsonArray().get(j).getAsJsonObject().get("subject").toString().replaceAll("\"", "");
                            String startDateTime = jsonElement.getAsJsonArray().get(j).getAsJsonObject().get("startDateTime").toString().replaceAll("\"", "");
                            String endDateTime = jsonElement.getAsJsonArray().get(j).getAsJsonObject().get("endDateTime").toString().replaceAll("\"", "");
                            String time = jsonElement.getAsJsonArray().get(j).getAsJsonObject().get("time").toString().replaceAll("\"", "");
                            String classroom = jsonElement.getAsJsonArray().get(j).getAsJsonObject().get("classroom").toString().replaceAll("\"", "");

                            concat = changeFormatDate(date) + " " + startDateTime + " - " + endDateTime + " (" + time + ") " + studentSubject + " " + classroom;

                            System.out.println("Concat"+concat);

                            if(!attendance.contains(concat)){
                                attendance.add(concat);
                            }
                        }
                    }
                }
            }
        }
        return attendance;
    }

    private void getStartCalendar() throws ParseException {

        View.OnClickListener showDatePicker = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final View vv = v;

                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        if (vv.getId() == R.id.calendar_start_spinner) {
                            startDate = Integer.toString(dayOfMonth)+"/"+Integer.toString(monthOfYear+1)+"/"+Integer.toString(year);
                            calendar_start.setText(startDate);
                            try {
                                long start = getMilliseconds(startDate);
                                startTime = start*10000000;
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        } else {
                            endDate = Integer.toString(dayOfMonth)+"/"+Integer.toString(monthOfYear+1)+"/"+Integer.toString(year);

                            if(endDate.compareTo(startDate) < 1){
                                new AlertDialog.Builder(getActivity())
                                        .setTitle("Fecha errónea")
                                        .setMessage("La fecha introducida es incorrecta")
                                        .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {

                                            }
                                        }).show();
                            }else {

                                calendar_end.setText(endDate);
                                try {
                                    long end = getMilliseconds(endDate);
                                    endTime = end * 10000000;
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        if(startTime != 0 && endTime != 0) {
                            getDataFromPubNub(startTime, endTime);
                        }
                    }
                }, Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
                datePickerDialog.show();
            }
        };
        calendar_start.setOnClickListener(showDatePicker);
        calendar_end.setOnClickListener(showDatePicker);
    }

    private long getMilliseconds(String string) throws ParseException {
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy", new Locale("es", "ES"));
        Date date = formatter.parse(string);
        return (date.getTime()) /1000;
    }

    public String changeFormatDate(String date) throws ParseException {
        SimpleDateFormat inFormat = new SimpleDateFormat("dd/MM/yyyy", new Locale("es", "ES"));
        SimpleDateFormat outFormat = new SimpleDateFormat("EEE, d MMM yyyy", new Locale("es", "ES"));
        return outFormat.format(inFormat.parse(date));
    }

}