package es.teacher_app.view.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.google.firebase.messaging.FirebaseMessaging;
import com.pubnub.api.PNConfiguration;
import com.pubnub.api.PubNub;
import com.pubnub.api.callbacks.PNCallback;
import com.pubnub.api.models.consumer.PNPublishResult;
import com.pubnub.api.models.consumer.PNStatus;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import es.core.interfaces.Calendar;
import es.core.EditTextProfileEvent;
import es.core.R;
import es.core.interfaces.RepositoryObserver;
import es.core.calendarTools.model.Schedule;
import es.core.calendarTools.model.ScheduleList;
import es.core.rest.ApiClient;
import es.core.rest.ApiInterface;
import es.core.interfaces.PubNubMessage;
import es.teacher_app.notification.UserDataRepository;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import static android.app.Activity.RESULT_OK;

public class Profile extends Fragment implements RepositoryObserver, Calendar, PubNubMessage {

    private EditText name;
    private EditText dni;
    private EditText email;
    private TextView subject;
    private TextView location;
    private TextView currentDate;
    private ImageView image;
    private View view;
    private static int RESULT_LOAD_IMAGE = 1;
    private UserDataRepository ud;
    private SharedPreferences preferences;
    private List<Schedule> schedules = new ArrayList<>();
    private String locationNow = "";
    public static String idC ="";
    private static final String TAG = Profile.class.getSimpleName();

    public Profile(){}

    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_profile, container, false);

        preferences = PreferenceManager.getDefaultSharedPreferences(getContext());

        inicializeViewElements();
        getViewElementsText();

        ud = UserDataRepository.getInstance(getActivity());
        ud.registerObserver(this);

        FirebaseMessaging.getInstance().subscribeToTopic("classroom");
        System.out.println("Subscribed to news topic");

        selectProfilePhoto();
        getProfilePhoto();
        getDate();

        return view;
    }

    private void selectProfilePhoto(){
        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkgallerypermission();
            }
        });
    }

    private void inicializeViewElements(){
        image = (ImageView) view.findViewById(R.id.image);
        name = (EditText) view.findViewById(R.id.name);
        dni = (EditText) view.findViewById(R.id.dni);
        email = (EditText) view.findViewById(R.id.email);
        subject = (TextView) view.findViewById(R.id.subject);
        location = (TextView) view.findViewById(R.id.location);
        currentDate = (TextView) view.findViewById(R.id.date);
    }

    private void getViewElementsText(){
        EditTextProfileEvent editTextProfileEvent = new EditTextProfileEvent(getActivity(), preferences);

        editTextProfileEvent.getText(name, "name");
        editTextProfileEvent.getText(dni, "dni");
        editTextProfileEvent.getText(email, "email");
    }

    private void checkgallerypermission() {
        int MY_READ_PERMISSION_REQUEST_CODE =1 ;
        if (ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.READ_EXTERNAL_STORAGE)== PackageManager.PERMISSION_GRANTED) {
            loadImagefromGallery(view);
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(
                        new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE},
                        MY_READ_PERMISSION_REQUEST_CODE);
            }
        }
    }

    public void loadImagefromGallery(View view) {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent, RESULT_LOAD_IMAGE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            PreferenceManager.getDefaultSharedPreferences(getContext()).edit().putString("gallery photo", picturePath).apply();
            cursor.close();

            System.out.println("Path "+BitmapFactory.decodeFile(picturePath));

            image.setImageBitmap(BitmapFactory.decodeFile(picturePath));
        }
    }

    private void getProfilePhoto(){
        String picturePath = PreferenceManager.getDefaultSharedPreferences(getContext()).getString("gallery photo", "");
        if(!picturePath.equals("")) {
            image.setImageBitmap(BitmapFactory.decodeFile(picturePath));
        }
    }

    @Override
    public void onUserDataChanged(String token) {
        if(!token.equals("Lugar")){
            getClassroom(token);
            getDataFromCalendar(token);
        } else{
            this.location.setText(token);
            this.subject.setText("Asignatura");
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ud.removeObserver(this);
    }

    private void getClassroom(final String token){
        final ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ScheduleList> call = apiService.getMyJSON();

        call.enqueue(new Callback<ScheduleList>() {
            @Override
            public void onResponse(Call<ScheduleList> call, Response<ScheduleList> response) {
                schedules = response.body().getSchedule();

                for (int i = 0; i < schedules.size(); i++) {
                    if(token.equals(schedules.get(i).getDescription())){
                        locationNow = schedules.get(i).getLocation();
                        location.setText(schedules.get(i).getLocation());
                        idC= schedules.get(i).getLocation();
                    }
                }
            }

            @Override
            public void onFailure(Call<ScheduleList> call, Throwable t) {
                Log.e(TAG, t.toString());
            }
        });
    }

    public String changeFormatDate(String date) throws ParseException {
        SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", new Locale("es", "ES"));
        SimpleDateFormat outFormat = new SimpleDateFormat("dd/MM/yyyy", new Locale("es", "ES"));
        return outFormat.format(inFormat.parse(date));
    }

    public Date changeFormatHour(String date) throws ParseException {
        SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", new Locale("es", "ES"));
        SimpleDateFormat outFormat = new SimpleDateFormat("HH:mm", new Locale("es", "ES"));
        String hourSchedule = outFormat.format(inFormat.parse(date));
        return outFormat.parse(hourSchedule);
    }

    public Date getHour() throws ParseException {
        DateFormat dateFormat = new SimpleDateFormat("HH:mm", new Locale("es", "ES"));
        Date newDate = new Date();
        String date = dateFormat.format(newDate);
        return dateFormat.parse(date);
    }

    public String getDate(){
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", new Locale("es", "ES"));
        Date newDate = new Date();
        String date = dateFormat.format(newDate);
        currentDate.setText(date);
        return date;
    }

    @Override
    public void getDataFromCalendar(final String token) {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ScheduleList> call = apiService.getMyJSON();

        call.enqueue(new Callback<ScheduleList>() {
            @Override
            public void onResponse(Call<ScheduleList> call, Response<ScheduleList> response) {
                schedules = response.body().getSchedule();
                for (int i = 0; i < schedules.size(); i++) {
                    try {
                        if((token.equals(schedules.get(i).getDescription()) && getDate().equals(changeFormatDate(schedules.get(i).getStartDate())))){
                           if((getHour().compareTo(changeFormatHour(schedules.get(i).getStartDate())) == 1 ||
                               getHour().compareTo(changeFormatHour(schedules.get(i).getStartDate())) == 0) &&
                                   (getHour().compareTo(changeFormatHour(schedules.get(i).getEndDate())) == -1 ||
                               getHour().compareTo(changeFormatHour(schedules.get(i).getEndDate())) == 0)){

                                subject.setText(schedules.get(i).getSummary());
                            }
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ScheduleList> call, Throwable t) {
                Log.e(TAG, t.toString());
            }
        });
    }

    @Override
    public void sendMessage(HashMap<String, Object> userData, String channelName) {
        PNConfiguration pnConfiguration = new PNConfiguration();
        pnConfiguration.setSubscribeKey("sub-c-b2c3168a-45f6-11e7-b730-0619f8945a4f");
        pnConfiguration.setPublishKey("pub-c-e9ba54f6-4e07-48aa-8a0e-087d3d0fc6d6");
        pnConfiguration.setSecure(false);

        PubNub pubnub = new PubNub(pnConfiguration);

        pubnub.publish()
                .message(userData)
                .channel(channelName)
                .shouldStore(true)
                .usePOST(true)
                .async(new PNCallback<PNPublishResult>() {
                    @Override
                    public void onResponse(PNPublishResult result, PNStatus status) {
                        if (status.isError()) {
                            // something bad happened.
                            System.out.println("error happened while publishing: " + status.toString());
                        } else {
                            System.out.println("publish worked! timetoken: " + result.getTimetoken());
                        }
                    }
                });
    }
}
