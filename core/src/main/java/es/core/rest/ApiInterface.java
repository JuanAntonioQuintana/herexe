package es.core.rest;

import es.core.calendarTools.model.ScheduleList;
import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiInterface {

    @GET("/calendar/v3/calendars/proyectofinal177@gmail.com/events?key=AIzaSyDTYw05UdS4G3dfQ9QHQBLBOq1MXLzUrMw")
    Call<ScheduleList> getMyJSON();
}
