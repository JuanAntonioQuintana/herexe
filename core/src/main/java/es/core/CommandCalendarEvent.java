package es.core;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import es.core.calendarTools.DividerItemDecoration;
import es.core.calendarTools.model.Schedule;

public class CommandCalendarEvent {
    private View view;
    private Context context;

    public CommandCalendarEvent(View view, Context context){
        this.view = view;
        this.context = context;
    }

    public RecyclerView initializeRecyclerView(){
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.schedule_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.addItemDecoration(
                new DividerItemDecoration(context, DividerItemDecoration.VERTICAL_LIST));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        return recyclerView;
    }

    public ProgressDialog getProgressDialog(){
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(context.getString(R.string.string_message));
        progressDialog.show();

        return progressDialog;
    }

    public List<Schedule> getSubjectNowResult(List<Schedule> schedules) throws ParseException {
        ArrayList<Schedule> schedulesR = new ArrayList<>();

        for (int i = 0; i < schedules.size(); i++) {
            if (changeFormatDate(schedules.get(i).getStartDate()).equals(getDate()) && (changeFormatHour(schedules.get(i).getStartDate()).compareTo(getHour()) == -1 || changeFormatHour(schedules.get(i).getStartDate()).compareTo(getHour()) == 0) && (changeFormatHour(schedules.get(i).getEndDate()).compareTo(getHour()) == 1 || changeFormatHour(schedules.get(i).getEndDate()).compareTo(getHour()) == 0)){
                schedulesR.add(schedules.get(i));
            }
        }

        return schedulesR;
    }

    private String changeFormatDate(String date) throws ParseException {

        SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", new Locale("es", "ES"));
        SimpleDateFormat outFormat = new SimpleDateFormat("dd MMMM yyyy", new Locale("es", "ES"));

        return outFormat.format(inFormat.parse(date));
    }

    private Date changeFormatHour(String date) throws ParseException {
        SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", new Locale("es", "ES"));
        SimpleDateFormat outFormat = new SimpleDateFormat("HH:mm", new Locale("es", "ES"));
        String hourSchedule = outFormat.format(inFormat.parse(date));
        Date hourSchedules = outFormat.parse(hourSchedule);
        return hourSchedules;
    }

    public String getDate(){
        DateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy", new Locale("es", "ES"));
        Date newDate = new Date();
        String date = dateFormat.format(newDate);
        return date;
    }

    private Date getHour() throws ParseException {
        DateFormat dateFormat = new SimpleDateFormat("HH:mm", new Locale("es", "ES"));
        Date newDate = new Date();
        String date = dateFormat.format(newDate);
        Date hour = dateFormat.parse(date);
        return hour;
    }
}
