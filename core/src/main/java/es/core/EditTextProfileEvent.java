package es.core;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EditTextProfileEvent {

    private Context context;
    private SharedPreferences preferences;

    public EditTextProfileEvent(Context context, SharedPreferences preferences) {
        this.context = context;
        this.preferences = preferences;

    }

    public void getText(final EditText editText, final String inputText) {

        editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_DONE||actionId==EditorInfo.IME_ACTION_NEXT) {
                    String text = editText.getText().toString();

                    if(validateText(inputText, text)){
                        preferences.edit().putString(inputText, text).apply();
                        editText.setFocusable(false);

                        hideKeyBoard(editText);
                    }else{
                        Toast.makeText(context,
                                "Compruebe que ha introducido correctamente el texto", Toast.LENGTH_SHORT).show();
                    }

                    handled = true;


                }
                return handled;
            }
        });

        if(!TextUtils.isEmpty(preferences.getString(inputText, ""))){
            editText.setText(preferences.getString(inputText, ""));
            editText.setFocusable(false);
        }
    }

    private boolean validateText(String inputText, String text){
        switch (inputText) {
            case "name":
                return validateName(text);
            case "dni":
                return validateDni(text);
            case "email":
                return validateEmail(text);
        }

       return false;
    }

    private boolean validateEmail(String mail){
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(mail).matches();
    }
    private boolean validateName(String name){
        String regx = "^[a-z áéíóúñüÁÉÍÓÚÑÜ]+$";
        Pattern pattern = Pattern.compile(regx,Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(name);
        return matcher.find();
    }
    private boolean validateDni(String dni){
        String regx = "^([0-9])*$";
        Pattern pattern = Pattern.compile(regx,Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(dni);
        return matcher.find();
    }

    private void hideKeyBoard(EditText editText){
        InputMethodManager imm = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }
}
