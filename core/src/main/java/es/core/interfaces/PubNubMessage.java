package es.core.interfaces;

import java.util.HashMap;

public interface PubNubMessage {

    void sendMessage(HashMap<String, Object> userData, String channelName);
}
