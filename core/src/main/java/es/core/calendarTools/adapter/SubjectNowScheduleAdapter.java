package es.core.calendarTools.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.List;
import es.core.R;
import es.core.calendarTools.model.Schedule;

public class SubjectNowScheduleAdapter extends RecyclerView.Adapter<SubjectNowScheduleAdapter.ScheduleViewHolder> {

    private List<Schedule> schedules;
    private int rowLayout;
    private Context context;

    static class ScheduleViewHolder extends RecyclerView.ViewHolder {
        LinearLayout itemLayout;
        TextView subject;
        TextView classroom;

        ScheduleViewHolder(View v) {
            super(v);
            itemLayout = (LinearLayout) v.findViewById(R.id.item_Layout);
            subject = (TextView) v.findViewById(R.id.textItem1);
            classroom = (TextView) v.findViewById(R.id.textItem2);
        }
    }

    public SubjectNowScheduleAdapter(List<Schedule> schedules, int rowLayout, Context context) {
        this.schedules = schedules;
        this.rowLayout = rowLayout;
        this.context = context;
    }

    @Override
    public SubjectNowScheduleAdapter.ScheduleViewHolder onCreateViewHolder(ViewGroup parent,
                                                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new ScheduleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ScheduleViewHolder holder, final int position) {
        holder.subject.setText(schedules.get(position).getSummary());
        holder.classroom.setText(schedules.get(position).getLocation());

    }

    @Override
    public int getItemCount() {
        return schedules.size();
    }
}