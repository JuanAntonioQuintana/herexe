package es.core.calendarTools.model;

import com.google.gson.annotations.SerializedName;

class DateTime {

    @SerializedName("dateTime")
    private String dateTime;

    String getDateTime() {
        return dateTime;
    }
}