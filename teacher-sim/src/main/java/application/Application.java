package application;

import model.Image;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONObject;
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class Application extends JFrame {
    private final Map<String, Rectangle> beacons = new HashMap<>();
    private String currentBeacon = null;

    public static void main(String[] args) {
        new Application();
    }

    private Application(){
        this.setTitle("Teacher Simulation");
        this.initBeacons();
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setSize(new Dimension(1000, 900));
        this.setLocationRelativeTo(null);
        this.createImagePanel();
        this.executeTimer();
        this.setVisible(true);
    }

    private void executeTimer() {
        new Timer().schedule(timerTask(), 1000, 1000);
    }

    private void initBeacons() {
        this.beacons.put("bd8b890c2c77f7b2", new Rectangle(22, 16, 268, 398));
        this.beacons.put("f1676cad3f0a853c", new Rectangle(278, 16, 334, 398));
        this.beacons.put("391b8378e2f5d950", new Rectangle(622, 16, 246, 398));
        this.beacons.put("55905c9a8da9e059", new Rectangle(22, 592, 326, 270));
        this.beacons.put("d342d05778d4ce70", new Rectangle(336, 592, 206, 270));
        this.beacons.put("9d2c245a50a2a97b", new Rectangle(552, 592, 158, 270));
    }

    private void createImagePanel(){
        this.getContentPane().add(imagePanel());
    }

    private ImagePanel imagePanel() {
        ImagePanel panel = new ImagePanel(image());
        panel.addMouseMotionListener(mouseListener());
        return panel;
    }

    private MouseMotionListener mouseListener() {
        return new MouseMotionListener() {
            @Override
            public void mouseDragged(MouseEvent e) {

            }

            @Override
            public void mouseMoved(MouseEvent e) {
                for (String key : beacons.keySet()) {
                    Rectangle rectangle = beacons.get(key);
                    if (!rectangle.contains(e.getX(), e.getY())) continue;
                    currentBeacon = key;
                    return;
                }
                currentBeacon = null;
            }
        };
    }

    private Image image(){
        return new FileImageReader("../herexe/sim/src/").read();
    }


    private TimerTask timerTask() {
        return new TimerTask() {
            @Override
            public void run() {
                if(currentBeacon != null){
                    sendMessage(currentBeacon);
                }

            }
        };
    }

    private void sendMessage(String currentBeacon) {
        HttpClient client = HttpClientBuilder.create().build();
        HttpPost post = new HttpPost("https://fcm.googleapis.com/fcm/send");
        post.setHeader("Content-type", "application/json");
        post.setHeader("Authorization", "key=AIzaSyDogboBVX_1qrcn17E6voNqAHjhynSkbus");

        JSONObject message = new JSONObject();
        message.put("to", "/topics/classroom");
        message.put("priority", "high");

        JSONObject notification = new JSONObject();
        notification.put("title", "Token");
        notification.put("body", currentBeacon);

        message.put("notification", notification);

        post.setEntity(new StringEntity(message.toString(), "UTF-8"));
        HttpResponse response = null;
        try {
            response = client.execute(post);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(response);
        System.out.println(message);
    }

}
