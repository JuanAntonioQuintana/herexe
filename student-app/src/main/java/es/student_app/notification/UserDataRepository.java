package es.student_app.notification;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;

import com.pubnub.api.PNConfiguration;
import com.pubnub.api.PubNub;
import com.pubnub.api.callbacks.PNCallback;
import com.pubnub.api.models.consumer.PNPublishResult;
import com.pubnub.api.models.consumer.PNStatus;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import es.core.interfaces.PubNubMessage;
import es.core.interfaces.RepositoryObserver;
import es.core.interfaces.Subject;
import es.core.calendarTools.model.Schedule;
import es.core.calendarTools.model.ScheduleList;
import es.core.rest.ApiClient;
import es.core.rest.ApiInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserDataRepository implements Subject, PubNubMessage {

    private long time;
    private String token;
    private String subject = "";
    private String subjectNow = "";
    private String location = "";
    private String start = "";
    private String end = "";
    private int timeEnable = 0;
    private String description = "";
    private static final String TAG = UserDataRepository.class.getSimpleName();
    private List<Schedule> schedules = new ArrayList<>();
    private ArrayList<RepositoryObserver> mObservers;
    private static UserDataRepository INSTANCE = null;
    private final Handler handler = new Handler();
    private HashMap<String, Object>  startEvent = new HashMap<>();
    private HashMap<String, Object>  endEvent = new HashMap<>();
    private String id;
    private String name;
    private String dni;
    private SharedPreferences preferences;

    private UserDataRepository(Context context) {
        mObservers = new ArrayList<>();
        updateData();
        receiveSignal(context);
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    private void getDataFromCalendar(){
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ScheduleList> call = apiService.getMyJSON();

        call.enqueue(new Callback<ScheduleList>() {
            @Override
            public void onResponse(Call<ScheduleList> call, Response<ScheduleList> response) {
                schedules = response.body().getSchedule();
                for (int i = 0; i < schedules.size(); i++) {
                    try {
                        if(changeFormatDate(schedules.get(i).getStartDate()).equals(getDate()) && (changeFormatHour(schedules.get(i).getStartDate()).compareTo(getHour()) == -1 ||
                                changeFormatHour(schedules.get(i).getStartDate()).compareTo(getHour()) == 0) &&
                                (changeFormatHour(schedules.get(i).getEndDate()).compareTo(getHour()) == 1)){
                            initialize(schedules.get(i).getDescription(), schedules.get(i).getLocation(), schedules.get(i).getSummary(), schedules.get(i).getStartDate(), schedules.get(i).getEndDate());
                            break;
                        } else{
                            initialize("", "", "", "", "");
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }


            }

            @Override
            public void onFailure(Call<ScheduleList> call, Throwable t) {
                Log.e(TAG, t.toString());
            }
        });
    }

    private void initialize(String description, String location, String subject, String start, String end){
        this.description = description;
        this.location = location;
        this.subject = subject;
        this.start = start;
        this.end = end;
    }

    private void receiveSignal(Context context) {

        BroadcastReceiver mReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                getDataFromCalendar();

                String text = intent.getStringExtra("token");

                System.out.println("La asignatura de ahora es:" + subject);
                System.out.println("La start de ahora es:" + start);
                System.out.println("La end de ahora es:" + end);

                if (timeEnable != 1 && !start.equals("") && !end.equals("") && !subject.equals("") &&
                        !TextUtils.isEmpty(preferences.getString("name", ""))
                        && !TextUtils.isEmpty(preferences.getString("dni", ""))) {

                    System.out.println("El tiempo se ha iniciado");

                    id = Settings.Secure.getString(context.getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
                    subjectNow = subject;
                    name = preferences.getString("name", "");
                    dni = preferences.getString("dni", "");

                    startEvent.put("classroom", location);
                    startEvent.put("id", id);
                    startEvent.put("name", name);
                    startEvent.put("dni", dni);
                    startEvent.put("subject", subject);

                    try {
                        startEvent.put("startDateTime", getCalendarHour(start));
                        startEvent.put("endDateTime", getCalendarHour(end));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    startEvent.put("date", getDate());
                    startEvent.put("start", System.currentTimeMillis());
                    startEvent.put("status", 0);

                    sendMessage(startEvent, "student_record");
                    timeEnable = 1;
                }

                if (timeEnable == 1 && !subject.equals(subjectNow)) {

                    System.out.println("El mensaje de end se ha enviado ha pubnup en onReceive");

                    endEvent.put("classroom", location);
                    endEvent.put("id", id);
                    endEvent.put("name", name);
                    endEvent.put("dni", dni);
                    endEvent.put("subject", subject);

                    try {
                        endEvent.put("startDateTime", getCalendarHour(start));
                        endEvent.put("endDateTime", getCalendarHour(end));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    endEvent.put("date", getDate());
                    endEvent.put("end", System.currentTimeMillis());
                    endEvent.put("status", 0);

                    sendMessage(endEvent, "student_record");
                    timeEnable = 0;
                }

                configureProfile(text);

                time = 0;
            }
        };
        context.registerReceiver(mReceiver, new IntentFilter("get token"));
    }

    private void configureProfile(String token){
        if(subject.equals("")){
            setUserData("Lugar");
        } else {
            setUserData(token);
        }
    }

    private void updateData() {
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        time += 1000;

                        if (time == 8000) {
                            if(timeEnable == 1){
                                System.out.println("El mensaje de end se ha enviado ha pubnup en updateData");

                                endEvent.put("classroom", location);
                                endEvent.put("id", id);
                                endEvent.put("subject", subject);

                                try {
                                    endEvent.put("startDateTime", getCalendarHour(start));
                                    endEvent.put("endDateTime", getCalendarHour(end));
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }

                                endEvent.put("date", getDate());
                                endEvent.put("end", System.currentTimeMillis());
                                endEvent.put("name", name);
                                endEvent.put("dni", dni);
                                endEvent.put("status", 0);

                                sendMessage(endEvent, "student_record");
                                timeEnable = 0;
                            }

                            setUserData("Lugar");
                        }

                        Log.d("TimerExample", "Going for... " + time);
                    }
                });
            }
        }, 1000, 1000);

    }

    public static UserDataRepository getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = new UserDataRepository(context);
        }
        return INSTANCE;
    }

    @Override
    public void registerObserver(RepositoryObserver repositoryObserver) {
        if(!mObservers.contains(repositoryObserver)) {
            mObservers.add(repositoryObserver);
        }
    }

    @Override
    public void removeObserver(RepositoryObserver repositoryObserver) {
        if(mObservers.contains(repositoryObserver)) {
            mObservers.remove(repositoryObserver);
        }
    }

    @Override
    public void notifyObservers() {
        for (RepositoryObserver observer: mObservers) {
            observer.onUserDataChanged(token);
        }
    }

    private void setUserData(String token) {
        this.token = token;
        notifyObservers();
    }

    private String changeFormatDate(String date) throws ParseException {
        SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", new Locale("es","ES"));
        SimpleDateFormat outFormat = new SimpleDateFormat("dd/MM/yyyy", new Locale("es","ES"));
        return outFormat.format(inFormat.parse(date));
    }

    private Date changeFormatHour(String date) throws ParseException {
        SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", new Locale("es","ES"));
        SimpleDateFormat outFormat = new SimpleDateFormat("HH:mm", new Locale("es","ES"));
        String hourSchedule = outFormat.format(inFormat.parse(date));
        return outFormat.parse(hourSchedule);
    }

    private Date getHour() throws ParseException {
        DateFormat dateFormat = new SimpleDateFormat("HH:mm", new Locale("es","ES"));
        Date newDate = new Date();
        String date = dateFormat.format(newDate);
        return dateFormat.parse(date);
    }

    private String getDate(){
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", new Locale("es","ES"));
        Date newDate = new Date();
        return dateFormat.format(newDate);
    }

    private String getCalendarHour(String date) throws ParseException {
        SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", new Locale("es","ES"));
        SimpleDateFormat outFormat = new SimpleDateFormat("HH:mm", new Locale("es","ES"));
        return outFormat.format(inFormat.parse(date));
    }

    @Override
    public void sendMessage(HashMap<String, Object> userData, String channelName) {
        PNConfiguration pnConfiguration = new PNConfiguration();
        pnConfiguration.setSubscribeKey("sub-c-b2c3168a-45f6-11e7-b730-0619f8945a4f");
        pnConfiguration.setPublishKey("pub-c-e9ba54f6-4e07-48aa-8a0e-087d3d0fc6d6");
        pnConfiguration.setSecure(false);

        PubNub pubnub = new PubNub(pnConfiguration);

        pubnub.publish()
                .message(userData)
                .channel(channelName)
                .shouldStore(true)
                .usePOST(true)
                .async(new PNCallback<PNPublishResult>() {
                    @Override
                    public void onResponse(PNPublishResult result, PNStatus status) {
                        if (status.isError()) {
                            System.out.println("error happened while publishing: " + status.toString());
                        } else {
                            System.out.println("publish worked! timetoken: " + result.getTimetoken());
                        }
                    }
                });
    }
}
